package com.agenosworld.frozen.mainmenu;

import java.util.ArrayList;

import com.agenosworld.frozen.CameraManager;
import com.agenosworld.frozen.GameLoop;
import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.BGColorManager;
import com.agenosworld.general.GameState;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class MainMenu extends GameState {
	
	// The Camera through which the Main Menu is viewed
	private CameraManager cam;
	private ArrayList<LevelButton> levelButtons;
	
	// Background image
	private AtlasRegion background;
	
	// Background color
	private BGColorManager voidScreenColor = new BGColorManager();
	
	public MainMenu() {
		cam = new CameraManager(3, 800, 600);
		cam.lookAt(2, 1.5f);
		
		// Create the buttons for the levels
		levelButtons = new ArrayList<LevelButton>();
		
		for (short i=1; i<=8; i++) {
			LevelButton b = new LevelButton(cam, "map"+i, i);
			levelButtons.add(b);
			GameLoop.iManager.addProcessor(b);
		}
		
		// Create the background
		background = SpriteManager.getTexRegion("menu-background");
	}

	@Override
	public void update(float delta) {
	}

	@Override
	public void render(boolean voidScreen) {
		if (voidScreen)
			voidScreenColor.voidScreen();
			
		SpriteBatch batch = cam.getBatch();
		
		batch.begin();
		
		batch.draw(background, -1, 0, 5, 3);
		
		for (LevelButton b : levelButtons)
			b.draw(batch);
		
		batch.end();
	}
	
	@Override
	public void resize(int width, int height) {
		cam.resize(width, height);
	}

	@Override
	public void dispose() {
		cam.dispose();
		
		for (LevelButton b : levelButtons)
			b.dispose();
	}

}
