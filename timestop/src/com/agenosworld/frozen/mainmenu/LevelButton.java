package com.agenosworld.frozen.mainmenu;

import com.agenosworld.frozen.CameraManager;
import com.agenosworld.frozen.GameLoop;
import com.agenosworld.frozen.ProgressTracker;
import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.general.Drawable;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

public class LevelButton implements InputProcessor, Drawable, Disposable {
	
	private OrthographicCamera cam;
	private AtlasRegion visual;
	private String map;
	private short lvl;
	
	private float width = 64f/200f, height = 64f/200f;
	private float x, y;
	
	public LevelButton(CameraManager cam, String map, short lvl) {
		this.cam = cam.getCamera();
		this.map = map;
		this.lvl = lvl;
		
		x = 2.25f + (1.25f * width) * ((lvl-1) % 4);
		y = 1.7f - (1.25f * height) * (float)Math.floor((lvl-1) / 4);
		
		if (ProgressTracker.haveSucceeded(lvl))
			visual = SpriteManager.getTexRegion("level"+lvl);
		else
			visual = SpriteManager.getTexRegion("level"+lvl+"-grey");
	}
	
	private void onClick() {
		GameLoop.loadLevel(map, lvl);
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(visual, x, y, width, height);
	}
	
	@Override
	public boolean touchUp(int touchX, int touchY, int pointer, int button) {
		// Detect if the mouse is over the object
		Vector3 mPos = new Vector3(touchX, touchY, 0);
		cam.unproject(mPos);
		if (mPos.x > x && mPos.x < (x+width) && mPos.y > y && mPos.y < (y+width)) {
			onClick();
			return true;
		}
		
		return false;
	}
	
	@Override
	public void dispose() {
		GameLoop.iManager.removeProcessor(this);
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	

	

}
