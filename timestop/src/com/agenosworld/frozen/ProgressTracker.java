package com.agenosworld.frozen;

import java.util.ArrayList;

public class ProgressTracker {
	
	private static ArrayList<Short> successes = new ArrayList<Short>();
	
	public static void registerSuccess(short level) {
		if (!haveSucceeded(level))
			successes.add(new Short(level));
	}
	
	public static boolean haveSucceeded(short level) {
		return successes.contains(new Short(level));
	}

}
