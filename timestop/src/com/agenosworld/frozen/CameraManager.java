package com.agenosworld.frozen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

public class CameraManager implements Disposable {
	
	public static final float RATIO = 1f/32f; // The ratio of DIPs to Pixels
	
	private OrthographicCamera camera; // The camera
	
	private float aspectRatio;
	private float viewHeight; // DIPs
	
	private SpriteBatch batch; // The SpriteBatch used for rendering sprites
	
	// True screen width and height (pixels);
	private int scrnWidth;
	private int scrnHeight;
	
	public CameraManager (float viewHeight, int width, int height) {
		this.viewHeight = viewHeight;
		
		camera = new OrthographicCamera(viewHeight, viewHeight);
		batch = new SpriteBatch();
		resize(width, height);
	}
	
	public void lookAt(Vector2 pos) {
		lookAt(pos.x, pos.y);
	}
	
	public void lookAt(float x, float y) {
		camera.position.set(x, y, 0);
		updateCamera();
	}
	
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	public SpriteBatch getBatch() {
		return batch;
	}

	public void resize(int width, int height) {
		aspectRatio = (float)width/(float)height;
		
		// Adjust the viewport of the camera to a new aspect ratio
		camera.viewportWidth = viewHeight*aspectRatio;
		camera.viewportHeight = viewHeight;
		
		// Define scrnWidth and scrnHeight
		scrnWidth = width;
		scrnHeight = height;
		
		// Update the camera
		updateCamera();
	}

	public void updateCamera() {
		GL10 gl = Gdx.graphics.getGL10();
		
		// Clear the GL and update the viewport
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gl.glViewport(0, 0, scrnWidth, scrnHeight);
		
		camera.update();
		camera.apply(gl);
		
		// Update the spritebatch
		batch.setProjectionMatrix(camera.combined);
	}

	@Override
	public void dispose() {
		// Call dispose on all entities
		batch.dispose();
	}
	
}
