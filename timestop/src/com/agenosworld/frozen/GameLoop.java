package com.agenosworld.frozen;

import aurelienribon.tweenengine.Tween;

import com.agenosworld.frozen.gameworld.BGColorManager;
import com.agenosworld.frozen.gameworld.ColorAlphaBlock.ColorAlphaBlockAccessor;
import com.agenosworld.frozen.gameworld.GameEndText;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.agenosworld.frozen.gameworld.Goal;
import com.agenosworld.frozen.gameworld.BGColorManager.BGColorManagerAccessor;
import com.agenosworld.frozen.gameworld.GameEndText.GameEndTextAccessor;
import com.agenosworld.frozen.gameworld.Goal.GoalAccessor;
import com.agenosworld.frozen.gameworld.blocks.DangerBlock;
import com.agenosworld.frozen.gameworld.blocks.GravityBlock;
import com.agenosworld.frozen.gameworld.blocks.Launcher;
import com.agenosworld.frozen.gameworld.blocks.StaticBlock;
import com.agenosworld.frozen.gameworld.blocks.GravityBlock.GravityBlockAccessor;
import com.agenosworld.frozen.gameworld.player.Player;
import com.agenosworld.frozen.gameworld.player.Player.PlayerAccessor;
import com.agenosworld.frozen.mainmenu.MainMenu;
import com.agenosworld.general.Disposer;
import com.agenosworld.general.ParticleEmitterMRL.Particle;
import com.agenosworld.general.ParticleEmitterMRL.ParticleAccessor;
import com.agenosworld.general.StateManager;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.files.FileHandle;

public class GameLoop implements ApplicationListener {
	
	public static boolean debug = false;
	
	// Input Manager
	public static InputMultiplexer iManager = new InputMultiplexer();
	
	public static Disposer disposer = new Disposer();
	
	private static StateManager sManager;
	
	// File Handles for Map
	private static FileHandle mapPack;
	
	@Override
	public void create() {
		// Add Tween Accessors
		addTweenAccessors();
		
		// Setup basic managers
		Gdx.input.setInputProcessor(iManager);
		SpriteManager.loadAtlas(Gdx.files.internal("assets/sprites/pack"));
		sManager = new StateManager(disposer);
		
		// Create reusable FileHandle
		mapPack = Gdx.files.internal("assets/map");
		
		// Open the Main Menu
		displayMainMenu();
	}
	
	public static void loadLevel(String level, short lvl) {
		sManager.gotoState(new GameWorld(Gdx.files.internal("assets/map/"+level+".tmx"), mapPack, lvl));
	}
	
	public static void displayMainMenu() {
		sManager.gotoState(new MainMenu());
	}
	
	private static void addTweenAccessors() {
		Tween.setCombinedAttributesLimit(4);
		
		Tween.registerAccessor(StaticBlock.class, new ColorAlphaBlockAccessor());
		Tween.registerAccessor(DangerBlock.class, new ColorAlphaBlockAccessor());
		Tween.registerAccessor(Launcher.class, new ColorAlphaBlockAccessor());
		Tween.registerAccessor(GravityBlock.class, new GravityBlockAccessor());
		Tween.registerAccessor(Player.class, new PlayerAccessor());
		Tween.registerAccessor(GameEndText.class, new GameEndTextAccessor());
		Tween.registerAccessor(BGColorManager.class, new BGColorManagerAccessor());
		Tween.registerAccessor(Goal.class, new GoalAccessor());
		Tween.registerAccessor(Particle.class, new ParticleAccessor());
	}

	@Override
	public void dispose() {
		sManager.dispose();
	}

	@Override
	public void render() {
		float delta = Gdx.graphics.getDeltaTime();
		
		// UPDATE LOOP
		disposer.update(delta);
		sManager.update(delta);
		
		// RENDER LOOP
		sManager.render(true);
	}

	@Override
	public void resize(int width, int height) {
		sManager.resize(width, height);
	}
	
	@Override
	public void pause() {
	}

	@Override
	public void resume() { /* NOTCALLED */ }
}
