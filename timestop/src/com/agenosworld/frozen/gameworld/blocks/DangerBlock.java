package com.agenosworld.frozen.gameworld.blocks;

import aurelienribon.tweenengine.Tween;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.Animateable;
import com.agenosworld.frozen.gameworld.ColorAlphaBlock;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.agenosworld.general.Drawable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Disposable;

public class DangerBlock implements Animateable, Disposable, Drawable, ColorAlphaBlock {
	
	// The game world
	private GameWorld world;
	
	// Box2d Properties
	private World b2World;
	private Body body;
	
	// Visual properties
	private AtlasRegion visual;
	private float colorAlpha = 0;
	
	
	public DangerBlock(int x, int y, GameWorld world, String blockType) {
		this.world = world;
		this.b2World = world.getB2World();
		
		// Create Box2d representation
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.5f, 0.3f, new Vector2(0, -.2f), 0);

		BodyDef bDef = new BodyDef();
		bDef.type = BodyType.StaticBody;
		bDef.position.set(x+.5f, y+.5f);
		bDef.fixedRotation = true;

		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.isSensor = true;

		body = b2World.createBody(bDef);
		body.createFixture(fDef);

		// Associate this object with the body
		body.setUserData(this);

		// Get the colored version
		visual = SpriteManager.getTexRegion(blockType);
	}

	@Override
	public float getColorAlpha() {
		return colorAlpha;
	}
	
	@Override
	public void setColorAlpha(float alpha) {
		colorAlpha = alpha;
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		Vector2 pos = new Vector2(body.getPosition());
		pos.x -= .5f;
		pos.y -= .5f;
		
		if (colorAlpha > 0) { // Draw colored visual
			batch.setColor(1, 1, 1, colorAlpha);
			batch.draw(visual, pos.x, pos.y, 1, 1);
			batch.setColor(Color.WHITE);
		} // Greyscale visual covered by TM Drawer
	}

	@Override
	public void dispose() {
		b2World.destroyBody(body);
	}

	@Override
	public void animate() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, .7f)
			.target(1)
			.start(world.getTweenManager());
	}

	@Override
	public void freeze() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, .7f)
			.target(0)
			.start(world.getTweenManager());

	}

}
