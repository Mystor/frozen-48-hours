package com.agenosworld.frozen.gameworld.blocks;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Linear;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.agenosworld.general.Updatable;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class GravityBlock extends Obstacle implements Updatable {
	
	// Box2d Values
	private BodyDef bDef;
	private FixtureDef fDef;
	private World b2World;
	private Body body;
	
	// The GameWorld
	private GameWorld gameWorld;
	
	// Visual properties
	private float colorAlpha = 0;
	private AtlasRegion coloredVisual;
	private AtlasRegion greyscaleVisual;
	
	// Falling
	private boolean falling = false;
	private ObstacleMap obsMap;
	private int x, y;
	private Tween fallingTween;
	
	public GravityBlock(int x, int y, GameWorld w, String blockType, ObstacleMap obsMap) {
		gameWorld = w;
		b2World = w.getB2World();
		this.obsMap = obsMap;
		this.x = x; this.y = y;
		
		// Create Box2d representation
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.5f, 0.5f);
		
		bDef = new BodyDef();
		bDef.type = BodyType.StaticBody;
		bDef.position.set(x+.5f, y+.5f);
		bDef.fixedRotation = true;
		
		fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.friction = .5f;
		fDef.density = 1;
		
		body = b2World.createBody(bDef);
		body.createFixture(fDef);
		
		// Associate this object with the body
		body.setUserData(this);
		
		// Get the colored version
		coloredVisual = SpriteManager.getTexRegion(blockType);
		greyscaleVisual = SpriteManager.getTexRegion(blockType + "-grey");
	}
	
	public float getColorAlpha() {
		return colorAlpha;
	}
	
	public void setColorAlpha(float alpha) {
		colorAlpha = alpha;
	}
	
	public float getY() {
		return body.getPosition().y;
	}
	
	public void setY(float newY) {
		Vector2 position = new Vector2(body.getPosition());
		position.y = newY;
		body.setTransform(position, 0);
	}
	
	public void stopFalling() {
		falling = false;
		fallingTween = null;
	}
	
	@Override
	public void update(float delta) {
		if (colorAlpha != 0) { // Not frozen!
			if (!falling) {
				if (obsMap.registerObstacle(x, y-1, this) == null) {
					obsMap.freeObstacle(x, y);
					y=y-1;
					
					// Begin Tween
					fallingTween = 
						Tween.to(this, GravityBlockAccessor.FALL_TWEEN, 0.1f)
						.target(getY()-1)
						.ease(Linear.INOUT)
						.setCallback(new TweenCallback() {
							@Override
							public void onEvent(int type, BaseTween<?> source) {
								if (type == TweenCallback.COMPLETE) {
									GravityBlock b = (GravityBlock)(source.getUserData());
									b.stopFalling();
								}
							}
						})
						.setUserData(this)
						.start();
					
					// Ensure that we start falling
					falling = true;
				}
			}
			
			// Continue falling!
			if (fallingTween != null)
				fallingTween.update(delta * colorAlpha);
		}
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		Vector2 pos = body.getPosition();
		float drawX = pos.x-.5f, drawY = pos.y-.5f;
		
		// Draw B&W visual
		if (colorAlpha < 1f)
			batch.draw(greyscaleVisual, drawX, drawY, 1, 1);
		
		// Draw colored visual
		if (colorAlpha > 0f) {
			batch.setColor(1, 1, 1, colorAlpha);
			batch.draw(coloredVisual, drawX, drawY, 1, 1);
			batch.setColor(1, 1, 1, 1);
		}
	}

	@Override
	public void animate() {
		Tween.to(this, GravityBlockAccessor.COLOR_ALPHA, 0.7f)
			.target(1.0f)
			.start(gameWorld.getTweenManager());
	}

	@Override
	public void freeze() {
		Tween.to(this, GravityBlockAccessor.COLOR_ALPHA, 0.7f)
			.target(0f)
			.start(gameWorld.getTweenManager());
	}

	@Override
	public void dispose() {
		b2World.destroyBody(body);
	}


	public static class GravityBlockAccessor implements TweenAccessor<GravityBlock> {
		
		public static final int COLOR_ALPHA = 1;
		public static final int FALL_TWEEN = 2;
		
		public GravityBlockAccessor() {}
		
		@Override
		public int getValues(GravityBlock target, int tweenType,
				float[] returnValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				returnValues[0] = target.getColorAlpha();
				return 1;
			case FALL_TWEEN:
				returnValues[0] = target.getY();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(GravityBlock target, int tweenType,
				float[] newValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				target.setColorAlpha(newValues[0]);
				break;
			case FALL_TWEEN:
				target.setY(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}

}
