package com.agenosworld.frozen.gameworld.blocks;

public class ObstacleMap {
	
	private Obstacle[][] obstacles;
	
	public ObstacleMap(int width, int height) {
		obstacles = new Obstacle[width][height];
	}
	
	/**
	 * Attempts to register an obstacle at the specified position.
	 * The function returns null if the registration is successful, otherwise it returns the obstacle which is blocking.
	 * @param x The x-coordinate to register
	 * @param y The y-coordinate to register
	 * @param o The object to register in this position
	 * @return Null if successful, otherwise the blocking obstacle
	 */
	public Obstacle registerObstacle(int x, int y, Obstacle o) {
		if (obstacles[x][y] == null) {
			obstacles[x][y] = o;
			return null;
		} else {
			return obstacles[x][y];
		}
	}
	
	public Obstacle checkObstacle(int x, int y) {
		return obstacles[x][y];
	}
	
	public void freeObstacle(int x, int y) {
		obstacles[x][y] = null;
	}

}
