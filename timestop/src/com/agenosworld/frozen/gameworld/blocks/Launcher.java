package com.agenosworld.frozen.gameworld.blocks;

import java.util.ArrayList;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.Animateable;
import com.agenosworld.frozen.gameworld.ColorAlphaBlock;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.agenosworld.general.Drawable;
import com.agenosworld.general.ParticleEmitterMRL;
import com.agenosworld.general.ParticleEmitterMRL.Particle;
import com.agenosworld.general.ParticleEmitterMRL.ParticleAccessor;
import com.agenosworld.general.ParticleEmitterMRL.ParticleController;
import com.agenosworld.general.Updatable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Disposable;

public class Launcher implements Animateable, Drawable, Updatable, Disposable, ColorAlphaBlock {
	
	// Box2d Stuff
	private Body body;
	
	// GameWOrld
	private GameWorld world;
	
	// Visual properties
	private AtlasRegion visual;
	private AtlasRegion visualGrey;
	private ParticleEmitterMRL partEmitter;
	private LauncherParticleController partController;
	private float colorAlpha = 0;
	private float x, y;
	
	
	public Launcher(int x, int y, GameWorld world) {
		this.world = world;
		
		// Get the visual
		visual = SpriteManager.getTexRegion("launcher");
		visualGrey = SpriteManager.getTexRegion("launcher-grey");
		
		this.x = x;
		this.y = y;
		
		// Create the B2Body
		BodyDef bDef = new BodyDef();
		bDef.position.set(x+.5f, y+.5f);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(.5f, .5f);
		
		FixtureDef fDef = new FixtureDef();
		fDef.isSensor = true;
		fDef.shape = shape;
		
		body = world.getB2World().createBody(bDef);
		body.createFixture(fDef);
		body.setUserData(this);
		
		partEmitter = new ParticleEmitterMRL(10);
		
		// Configure particle emitter
		partEmitter.visual = SpriteManager.getTexRegion("launcherparticle");
		partEmitter.color = new Color(Color.WHITE);
		partEmitter.color.a = 0;
		partEmitter.velocity = 10;
		partEmitter.life = .1f;
		partEmitter.spawnTime = 0.01f;
		partEmitter.position.set(x+0.5f, y);
		partEmitter.dimensions.set(.125f, .5f);
		
		// Set up the controller
		partController = new LauncherParticleController();
		partController.setColor(.3f, .3f, .8f);
		partController.setColorAlpha(colorAlpha);
		partEmitter.controller = partController;
		
		// The simulate some time to ensure that particles are spawned
		for (int i=0; i<10; i++) {
			partEmitter.update(.01f);
		}
	}
	
	@Override
	public float getColorAlpha() {
		return colorAlpha;
	}

	@Override
	public void setColorAlpha(float cAlpha) {
		colorAlpha = cAlpha;
		partController.setColorAlpha(cAlpha);
	}

	@Override
	public void draw(SpriteBatch batch) {
		// Draw greyscale version
		batch.draw(visualGrey, x, y, 1, 1);
		
		// Draw colored version
		batch.setColor(1, 1, 1, colorAlpha);
		batch.draw(visual, x, y, 1, 1);
		batch.setColor(1, 1, 1, 1);
		
		// Draw particles
		partEmitter.draw(batch);
	}
	
	@Override
	public void update(float delta) {
		partEmitter.update(delta*colorAlpha);
	}

	@Override
	public void animate() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, .7f)
			.target(1)
			.start(world.getTweenManager());
	}

	@Override
	public void freeze() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, .7f)
			.target(0)
			.start(world.getTweenManager());
	}

	@Override
	public void dispose() {
		world.getB2World().destroyBody(body);
	}
	
	public class LauncherParticleController implements ParticleController {

		// Color properties
		private Color color;
		private Color currentColor = new Color();
		private float greyScale;
		
		private ArrayList<Particle> managedParticles = new ArrayList<Particle>();
		
		public void setColor(float r, float g, float b) {
			// Determine greyScale color;
			color = new Color(r, g, b, 1);
			greyScale = (color.r*.3f) + (color.g*.59f) + (color.b*.11f);
		}
		
		public void setColorAlpha(float cAlpha) {
			// Calculate the RGB values for the particle
			currentColor.r = color.r + (greyScale - color.r)*(1-cAlpha);
			currentColor.g = color.g + (greyScale - color.g)*(1-cAlpha);
			currentColor.b = color.b + (greyScale - color.b)*(1-cAlpha);
			
			// Set the color of the particles
			for (Particle p : managedParticles)
				p.setColor(currentColor.r, currentColor.g, currentColor.b);
		}
		
		private void setParticleColorAlpha(Particle particle) {
			particle.setColor(currentColor.r, currentColor.g, currentColor.b);
		}
		
		@Override
		public void manageParticle(Particle particle,
				ParticleEmitterMRL emitter, TweenManager manager) {
			
			// Set the color of the particle
			setParticleColorAlpha(particle);
			
			// Set the position of the particle to a varying value
			particle.getPosition().x += (Math.random()-.5f);
			
			// Set the dimension of the particle to a varying value
			particle.getDimensions().y += (Math.random() -.5f)*0.4;
			
			// Alpha animation
			Timeline.createSequence()
				.push(Tween.set(particle, ParticleAccessor.ALPHA)
					.target(0))
				.push(Tween.to(particle, ParticleAccessor.ALPHA, 0.02f)
					.target(.6f))
				.pushPause(0.06f)
				.push(Tween.to(particle, ParticleAccessor.ALPHA, 0.02f)
					.target(0))
				.start(manager);
			
			managedParticles.add(particle);
		}

		@Override
		public void unManageParticle(Particle particle,
				ParticleEmitterMRL emitter, TweenManager manager) {
			managedParticles.remove(particle);
		}
		
	}

}
