package com.agenosworld.frozen.gameworld.blocks;

import aurelienribon.tweenengine.Tween;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.ColorAlphaBlock;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class StaticBlock extends Obstacle implements ColorAlphaBlock {
	
	// Box2d Values
	private World b2World;
	private Body body;

	// The GameWorld
	private GameWorld gameWorld;
	
	// Visual properties
	String bType;
	float colorAlpha = 0;
	AtlasRegion coloredVisual;
	int x, y;
	
	public StaticBlock(int x, int y, GameWorld w, String blockType) {
		gameWorld = w;
		b2World = w.getB2World();
		bType = blockType;
		this.x = x;
		this.y = y;
		
		// Create Box2d representation
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.5f, 0.5f);
		
		BodyDef bDef = new BodyDef();
		bDef.type = BodyType.StaticBody;
		bDef.position.set(x+.5f, y+.5f);
		
		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.friction = .5f;
		
		body = b2World.createBody(bDef);
		body.createFixture(fDef);
		
		// Associate this object with the body
		body.setUserData(this);
		
		// Get the colored version
		coloredVisual = SpriteManager.getTexRegion(blockType);
	}
	
	@Override
	public float getColorAlpha() {
		return colorAlpha;
	}
	
	@Override
	public void setColorAlpha(float alpha) {
		colorAlpha = alpha;
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		if (colorAlpha > 0f) {
			batch.setColor(1, 1, 1, colorAlpha);
			batch.draw(coloredVisual, x, y, 1, 1);
			batch.setColor(1, 1, 1, 1);
		} // Non-colored version drawn by Tiled Map Renderer
	}

	@Override
	public void animate() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, 0.7f)
			.target(1.0f)
			.start(gameWorld.getTweenManager());
	}

	@Override
	public void freeze() {
		Tween.to(this, ColorAlphaBlockAccessor.COLOR_ALPHA, 0.7f)
			.target(0f)
			.start(gameWorld.getTweenManager());
	}

	@Override
	public void dispose() {
		b2World.destroyBody(body);
	}


	/*public static class StaticBlockAccessor implements TweenAccessor<StaticBlock> {
		
		public static final int COLOR_ALPHA = 1;
		
		public StaticBlockAccessor() {}
		
		@Override
		public int getValues(StaticBlock target, int tweenType,
				float[] returnValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				returnValues[0] = target.getColorAlpha();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(StaticBlock target, int tweenType,
				float[] newValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				target.setColorAlpha(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}*/

}
