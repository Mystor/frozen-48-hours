package com.agenosworld.frozen.gameworld;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.general.Drawable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenCallback;

public class Goal implements Animateable, Drawable {
	
	private static final String CHILD_COLOR = "child";
	private static final String CHILD_GREY = "child-grey";
	private final float height = 2, width = 1;
	
	private GameWorld world;
	
	// Box2d Values
	private Body body;
	
	// Visual properties
	private AtlasRegion visual, visualGrey;
	private float colorAlpha = 0;
	private float x, y;
	
	public Goal(int x, int y, GameWorld world) {
		this.world = world;
		this.x = x;
		this.y = y;
		
		// Create the B2D representation
		BodyDef bDef = new BodyDef();
		bDef.position.set(x + width/2, y + height/2);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width/2, height/2);
		
		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.isSensor = true;
		
		// Attach to the world
		body = world.getB2World().createBody(bDef);
		body.setUserData(this);
		
		body.createFixture(fDef);
		
		// Get the visual properties
		visual = SpriteManager.getTexRegion(CHILD_COLOR);
		visualGrey = SpriteManager.getTexRegion(CHILD_GREY);
	}
	
	public float getColorAlpha() {
		return colorAlpha;
	}

	public void setColorAlpha(float f) {
		colorAlpha = f;
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		if (colorAlpha < 1)
			batch.draw(visualGrey, x, y, width, height);
		
		if (colorAlpha > 0) {
			batch.setColor(1, 1, 1, colorAlpha);
			batch.draw(visual, x, y, width, height);
			batch.setColor(Color.WHITE);
		}
	}
	
	@Override
	public void animate() {
		Tween.to(this, GoalAccessor.COLOR_ALPHA, 2)
			.target(1)
			.setCallback(new TweenCallback() {
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					if (type == TweenCallback.COMPLETE) {
						GameWorld world = (GameWorld)(source.getUserData());
						world.declareVictory(); // The goal has been animated, declare victory!
					}
				}
			})
			.setUserData(world)
			.start(world.getTweenManager());
	}

	@Override
	public void freeze() {
		world.getTweenManager().killTarget(this, GoalAccessor.COLOR_ALPHA); // Ensure that you don't win incorrectly
		
		Tween.to(this, GoalAccessor.COLOR_ALPHA, 2)
			.target(0)
			.start(world.getTweenManager());
	}
	
	public static class GoalAccessor implements TweenAccessor<Goal> {
		
		public static final int COLOR_ALPHA = 1;

		@Override
		public int getValues(Goal target, int tweenType, float[] returnValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				returnValues[0] = target.getColorAlpha();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(Goal target, int tweenType, float[] newValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				target.setColorAlpha(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}

}
