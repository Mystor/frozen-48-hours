package com.agenosworld.frozen.gameworld;

import aurelienribon.tweenengine.TweenAccessor;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.player.Player;
import com.agenosworld.general.Drawable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;

public class GameEndText implements Drawable {
	
	private AtlasRegion visual;
	private Player player;
	
	private float width = 12, height = 3;
	
	private float alpha = 0;
	
	public GameEndText(boolean victory, Player player) {
		this.player = player;
		
		if (victory)
			visual = SpriteManager.getTexRegion("text-victory");
		else
			visual = SpriteManager.getTexRegion("text-defeat");
	}
	
	public float getAlpha() {
		return alpha;
	}
	
	public void setAlpha(float a) {
		alpha = a;
	}

	@Override
	public void draw(SpriteBatch batch) {
		Vector2 renderPos = new Vector2(player.getBody().getPosition());
		renderPos.x -= width/2;
		renderPos.y += height/2;
		
		batch.setColor(1, 1, 1, alpha);
		batch.draw(visual, renderPos.x, renderPos.y, width, height);
		batch.setColor(Color.WHITE);
	}
	
	public static class GameEndTextAccessor implements TweenAccessor<GameEndText> {
		
		public static final int ALPHA = 1;

		@Override
		public int getValues(GameEndText target, int tweenType,
				float[] returnValues) {
			switch (tweenType) {
			case ALPHA:
				returnValues[0] = target.getAlpha();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(GameEndText target, int tweenType,
				float[] newValues) {
			switch (tweenType) {
			case ALPHA:
				target.setAlpha(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}

}
