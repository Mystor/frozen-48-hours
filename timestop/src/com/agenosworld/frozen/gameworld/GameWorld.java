package com.agenosworld.frozen.gameworld;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

import com.agenosworld.frozen.CameraManager;
import com.agenosworld.frozen.GameLoop;
import com.agenosworld.frozen.ProgressTracker;
import com.agenosworld.frozen.gameworld.BGColorManager.BGColorManagerAccessor;
import com.agenosworld.frozen.gameworld.GameEndText.GameEndTextAccessor;
import com.agenosworld.frozen.gameworld.blocks.DangerBlock;
import com.agenosworld.frozen.gameworld.blocks.GravityBlock;
import com.agenosworld.frozen.gameworld.blocks.Launcher;
import com.agenosworld.frozen.gameworld.blocks.ObstacleMap;
import com.agenosworld.frozen.gameworld.blocks.StaticBlock;
import com.agenosworld.frozen.gameworld.player.Player;
import com.agenosworld.frozen.gameworld.player.PlayerController;
import com.agenosworld.frozen.gameworld.player.Player.PlayerAccessor;
import com.agenosworld.general.ContactManager;
import com.agenosworld.general.GameState;
import com.agenosworld.general.Resizable;
import com.agenosworld.general.Updatable;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.TileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public class GameWorld extends GameState implements Disposable, Updatable, Resizable {
	
	// The current level id
	private short lvl;
	
	// The camera through which the game world is viewed
	private CameraManager cam;
	
	// Whether or not to update the scene
	private boolean gameRunning = true;
	
	// Box2D World
	private World b2World;
	private ContactManager cManager;
	
	// Map Loader
	private TiledMap map;
	private TileAtlas mapAtlas;
	private TileMapRenderer mapRenderer;
	
	// Debugging values
	private Box2DDebugRenderer b2Debug;

	// The Player
	private Player player;
	private PlayerController pController;
	
	// The ObstacleMap for GravityBlocks
	private ObstacleMap obsMap;
	
	// Blocks
	private Array<StaticBlock> staticBlocks;
	private Array<GravityBlock> gravityBlocks;
	private Array<DangerBlock> dangerBlocks;
	private Array<Launcher> launchers;
	
	// The goal
	private Goal goal;
	
	// Tween Manager
	private TweenManager tweenManager;

	// Game End Text
	private GameEndText gameEndText;
	
	// Background color
	private BGColorManager voidScreenColor = new BGColorManager();
	
	public GameWorld(FileHandle mapFile, FileHandle atlasDir, short lvl) {
		this.lvl = lvl;
		
		// Create the Managers
		cam = new CameraManager(18.75f, 800, 600);
		tweenManager = new TweenManager();
		
		// Load the map
		map = TiledLoader.createMap(mapFile);
		mapAtlas = new TileAtlas(map, atlasDir);
		
		// Create the renderer
		mapRenderer = new TileMapRenderer(map, mapAtlas, 16, 16, 1, 1);
		
		// Add the contact Manager
		cManager = new ContactManager();
		
		// Create the B2World
		createB2World();
		
		// Add the Contact Manager
		b2World.setContactListener(cManager);
	}

	public World getB2World() {
		return b2World;
	}
	
	public CameraManager getCameraManager() {
		return cam;
	}
	
	public ContactManager getContactManager() {
		return cManager;
	}
	
	public TweenManager getTweenManager() {
		return tweenManager;
	}
	
	public void killPlayer() {
		if (!gameRunning) // Already winning/losing
			return;
		
		gameRunning = false;
		// Destroy the Player Controller
		GameLoop.disposer.queueToDispose(pController);
		pController = null;
		
		// Grey out the player
		
		Tween.to(player, PlayerAccessor.COLOR_ALPHA, 2)
			.target(0f)
			.start(tweenManager);
		
		// Create the Defeat text
		gameEndText = new GameEndText(false, player);
		
		// Animate the Victory text
		Timeline.createSequence()
			.push(Tween.to(gameEndText, GameEndTextAccessor.ALPHA, 3)
				.target(1))
			.push(Tween.to(gameEndText, GameEndTextAccessor.ALPHA, 2)
				.target(1)) // DELAY
			.setCallback(new TweenCallback() {
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					if (type == TweenCallback.COMPLETE) {
						GameLoop.displayMainMenu(); // Game is over - return to the main menu
					}
				}
			})
			.start(tweenManager);
		
		// Freeze all blocks on screen
		for (int i=0; i<gravityBlocks.size; i++)
			gravityBlocks.get(i).freeze();
		for (int i=0; i<staticBlocks.size; i++)
			staticBlocks.get(i).freeze();
		for (int i=0; i<dangerBlocks.size; i++)
			dangerBlocks.get(i).freeze();
	}
	
	public void declareVictory() {
		if (!gameRunning) // Already winning/losing
			return;
		
		gameRunning = false;
		
		// Destroy the Player Controller
		GameLoop.disposer.queueToDispose(pController);
		pController = null;
		
		// Make the background blue
		Tween.to(voidScreenColor, BGColorManagerAccessor.COLOR_RGB, 1)
			.target(0.48627f, 1, 1)
			.start(tweenManager);
		
		// Create the Victory text
		gameEndText = new GameEndText(true, player);
		
		// Animate the Victory text
		Timeline.createSequence()
			.push(Tween.to(gameEndText, GameEndTextAccessor.ALPHA, 3)
				.target(1))
			.push(Tween.to(gameEndText, GameEndTextAccessor.ALPHA, 2)
				.target(1)) // DELAY
			.setCallback(new TweenCallback() {
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					if (type == TweenCallback.COMPLETE) {
						// Register success on this level!
						short lvl = ((Short)(source.getUserData())).shortValue();
						ProgressTracker.registerSuccess(lvl);
						
						GameLoop.displayMainMenu(); // Game is over - return to the main menu
					}
				}
			})
			.setUserData(new Short(lvl))
			.start(tweenManager);
		
		// Brighten the scene
		for (int i=0; i<gravityBlocks.size; i++)
			gravityBlocks.get(i).animate();
		for (int i=0; i<staticBlocks.size; i++)
			staticBlocks.get(i).animate();
		for (int i=0; i<dangerBlocks.size; i++)
			dangerBlocks.get(i).animate();
	}
	
	@Override
	public void update(float delta) {
		// Update the visual tweens
		tweenManager.update(delta);
		
		if (gameRunning) {
			// Update the gravityBlocks (let them fall!)
			for (int i=0; i<gravityBlocks.size; i++) {
				gravityBlocks.get(i).update(delta);
			}
		
			// Move the world
			b2World.step(delta, 1, 1);
		
			// Update the player
			if (player != null)
				player.update(delta);
			else
				throw new NullPointerException("No player spawn location");
			
			for (int i=0; i<launchers.size; i++)
				launchers.get(i).update(delta);
		}
	}
	
	@Override
	public void render(boolean voidScr) {
		// Void the screen
		if (voidScr)
			voidScreenColor.voidScreen();
		
		// Draw the player's aura
		SpriteBatch batch = cam.getBatch();
		
		batch.begin();
		player.drawAura(batch);
		batch.end();
		
		// Draw the map
		mapRenderer.render(cam.getCamera(), new int[] {1, 3} /* Only draw static blocks and danger blocks*/);
		
		// Render other sprites
		batch.begin();
		
		for (int i=0; i<staticBlocks.size; i++) // Draw static blocks
			staticBlocks.get(i).draw(batch); 
		
		for (int i=0; i<dangerBlocks.size; i++)
			dangerBlocks.get(i).draw(batch);
		
		for (int i=0; i<gravityBlocks.size; i++)// Draw gravity blocks
			gravityBlocks.get(i).draw(batch); 
		
		for (int i=0; i<launchers.size; i++)
			launchers.get(i).draw(batch);
		
		if (goal != null)
			goal.draw(batch); // Draw goal
		else
			throw new NullPointerException("No goal in World");
		
		if (player != null)
			player.draw(batch); // Draw player
		else
			throw new NullPointerException("No player spawn location");
		
		// If GameEndText is avaliable, render
		if (gameEndText != null)
			gameEndText.draw(batch);
		
		batch.end();
		
		// Render the debug for the B2World
		if (GameLoop.debug)
			b2Debug.render(b2World, cam.getCamera().combined);
		
	}
	
	@Override
	public void dispose() {
		// Dispose of all entities
		cam.dispose();
		b2World.dispose();
		if (pController != null)
			pController.dispose();
	}
	
	@Override
	public void resize(int width, int height) {
		cam.resize(width, height);
	}
	
	public void createB2World() {
		// Define constants
		b2World = new World(new Vector2(0, -10), true);
		
		// Get the map layer data
		int[][] fixedTiles = map.layers.get(1).tiles;
		int[][] gravityTiles = map.layers.get(2).tiles;
		int[][] dangerTiles = map.layers.get(3).tiles;
		int[][] specialTiles = map.layers.get(4).tiles;
		
		// Loop through the tiles
		int height = fixedTiles.length;
		int width = fixedTiles[0].length;
		
		// Create the ObstacleMap
		obsMap = new ObstacleMap(width, height);
		
		// Create the ArrayLists
		staticBlocks = new Array<StaticBlock>(width*height/5); // Arbitrary size
		gravityBlocks = new Array<GravityBlock>(width*height/10);
		dangerBlocks = new Array<DangerBlock>(width*height/25);
		launchers = new Array<Launcher>(5);
		
		for (int arrY=0; arrY<height; arrY++) {
			int y = height-arrY-1;
			for (int x=0; x<width; x++) {
				// For each tile in the map, perform actions to add physics items
				if (fixedTiles[arrY][x] != 0) { // Add fixed walls
					String blockType = map.getTileProperty(fixedTiles[arrY][x], "blockType");
					StaticBlock b = new StaticBlock(x, y, this, blockType);
					obsMap.registerObstacle(x, y, b);
					staticBlocks.add(b);
				}
				
				if (gravityTiles[arrY][x] != 0) { // Add gravity-affected blocks
					String blockType = map.getTileProperty(gravityTiles[arrY][x], "blockType");
					GravityBlock g = new GravityBlock(x, y, this, blockType, obsMap);
					obsMap.registerObstacle(x, y, g);
					gravityBlocks.add(g);
				}
				
				if (dangerTiles[arrY][x] != 0) {
					String blockType = map.getTileProperty(dangerTiles[arrY][x], "blockType");
					DangerBlock d = new DangerBlock(x, y, this, blockType);
					dangerBlocks.add(d);
				}
				
				if (specialTiles[arrY][x] != 0) { // Add special tiles
					String type = map.getTileProperty(specialTiles[arrY][x], "type");
					
					if (type.equals("goal") && goal == null) { // Add the goal
						goal = new Goal(x, y, this);
					}
					
					if (type.equals("player") && player == null) { // Add the player
						player = new Player(x, y, this);
						cManager.addProcessor(player);
						pController = new PlayerController(player);
						GameLoop.iManager.addProcessor(pController);
					}
					
					if (type.equals("launcher")) {
						System.out.println("foo");
						launchers.add(new Launcher(x, y, this));
					}
					
				}
			}
		}
		
		if (GameLoop.debug)
			b2Debug = new Box2DDebugRenderer();
	}

}
