package com.agenosworld.frozen.gameworld;

import aurelienribon.tweenengine.TweenAccessor;

public interface ColorAlphaBlock {

	public float getColorAlpha();
	
	public void setColorAlpha(float cAlpha);
	
	public static class ColorAlphaBlockAccessor implements TweenAccessor<ColorAlphaBlock> {
		
		public static final int COLOR_ALPHA = 1;

		@Override
		public int getValues(ColorAlphaBlock target, int tweenType,
				float[] returnValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				returnValues[0] = target.getColorAlpha();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(ColorAlphaBlock target, int tweenType,
				float[] newValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				target.setColorAlpha(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
		
		
	}
}
