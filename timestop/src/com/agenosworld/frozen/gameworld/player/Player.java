package com.agenosworld.frozen.gameworld.player;

import java.util.List;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;

import com.agenosworld.frozen.SpriteManager;
import com.agenosworld.frozen.gameworld.GameWorld;
import com.agenosworld.frozen.gameworld.blocks.DangerBlock;
import com.agenosworld.frozen.gameworld.blocks.GravityBlock;
import com.agenosworld.frozen.gameworld.blocks.Launcher;
import com.agenosworld.frozen.gameworld.blocks.Obstacle;
import com.agenosworld.general.Drawable;
import com.agenosworld.general.Updatable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Disposable;

public class Player implements Updatable, Drawable, Disposable, ContactListener {
	
	public enum Facing {LEFT, RIGHT};
	
	private static final String CHAR_SPR = "bearanim";
	private static final String GREY_SPR = "bearanim-grey";
	private static final String AURA_SPR = "timeaura";
	
	private static final float WALK_FORCE = 10.0f;
	private static final float JUMP_IMPULSE = 8f;
	private static final float MAX_SPEED = 3;
	
	// Visual Properties
	private AtlasRegion visual;
	private AtlasRegion visualGrey;
	private AtlasRegion auraVisual;
	private float width = 1;
	private float height = 2;
	private float colorAlpha = 1f;

	// Animation
	private float animProgress = 0f;
	private List<AtlasRegion> visuals;
	private List<AtlasRegion> visualsGrey;
	private Tween animTween;
	
	// Context Properties
	private GameWorld world;
	private World b2World;
	
	// Player Body and Fixture
	private Body body;
	private Fixture fixtureFeet;
	private Fixture fixtureHead;
	
	// Jumping
	private int canJump = 0;
	private int launchers = 0;
	
	// Death
	private int blocksOnHead = 0;
	private boolean touchingSpikes = false;
	
	// Time Aura
	private AnimationAura aura;
	
	// Moving Values
	private boolean walkL = false, walkR = false;
	private Facing facing = Facing.RIGHT;
	
	public Player(float x, float y, GameWorld world) {
		// Set values
		visuals = SpriteManager.getTexRegions(CHAR_SPR);
		visualsGrey = SpriteManager.getTexRegions(GREY_SPR);
//		visual = SpriteManager.getTexRegion(CHAR_SPR);
//		visualGrey = SpriteManager.getTexRegion(GREY_SPR);
		auraVisual = SpriteManager.getTexRegion(AURA_SPR);
		b2World = world.getB2World();
		this.world = world;
		
		// Create the B2Body
		createB2Body(x, y);
		body.setUserData(this); 
		
		// Attach the TimeAura
		aura = new AnimationAura(this);
		world.getContactManager().addProcessor(aura);
		
		// Start creating the animTween
		animTween = Tween.to(this, PlayerAccessor.ANIM_PROGRESS, .2f)
			.target(29f)
			.repeatYoyo(Tween.INFINITY, 0)
			.start();
	}
	
	public Body getBody() {
		return body;
	}
	
	public GameWorld getWorld() {
		return world;
	}
	
	public World getB2World() {
		return b2World;
	}
	
	public float getColorAlpha() {
		return colorAlpha;
	}
	
	public void setColorAlpha(float a) {
		colorAlpha = a;
	}
	
	public float getAnimProgress() {
		return animProgress;
	}
	
	public void setAnimProgress(float a) {
		animProgress = a;
	}
	
	public void startWalking(Facing f) {
		if (f == Facing.LEFT)
			walkL = true;
		else
			walkR = true;
		
		// Set the facing
		facing = f;
	}
	
	public void stopWalking(Facing f) {
		if (f == Facing.LEFT)
			walkL = false;
		else
			walkR = false;
	}

	public void tryJump() {
		if (canJump >= 1)
			body.applyLinearImpulse(new Vector2(0, JUMP_IMPULSE), body.getPosition());
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		Vector2 pos = body.getPosition();
		float x = pos.x-width/2;
		float y = pos.y-height/2;
		
		if (facing == Facing.LEFT) {
			batch.draw(visual, x, y, width/2, 0, width, height, -1, 1, 0);
			if (colorAlpha < 1) { // Draw dying animation
				batch.setColor(1, 1, 1, 1f-colorAlpha);
				batch.draw(visualGrey, x, y, width/2, 0, width, height, -1, 1, 0);
				batch.setColor(Color.WHITE);
			}
		} else {
			batch.draw(visual, x, y, width, height);
			if (colorAlpha < 1) { // Draw dying animation
				batch.setColor(1, 1, 1, 1f-colorAlpha);
				batch.draw(visualGrey, x, y, width, height);
				batch.setColor(Color.WHITE);
			}
		}
	}
	
	public void drawAura(SpriteBatch batch) {
		Vector2 pos = body.getPosition();
		float x = pos.x - 2.5f;
		float y = pos.y - 2.5f;
		
		batch.setColor(1, 1, 1, colorAlpha);
		batch.draw(auraVisual, x, y, 5, 5);
		batch.setColor(Color.WHITE);
	}
	
	@Override
	public void update(float delta) {
		// Move
		if (walkL) {
			body.applyForceToCenter(-WALK_FORCE, 0);
		}
		if (walkR) {
			body.applyForceToCenter(WALK_FORCE, 0);
		}
		
		// Limit player motion
		Vector2 linearVel = new Vector2(body.getLinearVelocity());
		if (linearVel.x > MAX_SPEED) {
			linearVel.x = MAX_SPEED;
		} else if (linearVel.x < -MAX_SPEED) {
			linearVel.x = -MAX_SPEED;
		}
		body.setLinearVelocity(linearVel);
		
		// Move the camera
		world.getCameraManager().lookAt(body.getPosition());
		
		// Move the Time Aura
		aura.update(delta);
		
		// Check to see if player is squashed
		if (canJump >= 1 && blocksOnHead >= 1)
			world.killPlayer();
		if (touchingSpikes)
			world.killPlayer();
		
		// Launch the player
		if (launchers >= 1)
			body.applyForceToCenter(0, 150);
		
		// Check to see if player has left the screen
		if (body.getPosition().y < -20)
			world.killPlayer();
		
		// Run the anim tween
		animTween.update(delta * (Math.abs(body.getLinearVelocity().x)/MAX_SPEED));
		
		// Set the current visuals
		int frame = Math.round(animProgress);
		visual = visuals.get(frame);
		visualGrey = visualsGrey.get(frame);
	}
	
	private void createB2Body(float x, float y) {
		BodyDef bDef = new BodyDef();
		bDef.type = BodyType.DynamicBody;
		bDef.position.set(x+.5f, y+1); // Position Person correctly
		bDef.fixedRotation = true;
		bDef.angularDamping = 2;
		
		// Body
		PolygonShape shapeBody = new PolygonShape();
		shapeBody.setAsBox(.4f, .7f);
		
		FixtureDef fDefBody = new FixtureDef();
		fDefBody.shape = shapeBody;
		fDefBody.density = 1;
		
		// Feet
		CircleShape shapeFeet = new CircleShape();
		shapeFeet.setRadius(.35f);
		shapeFeet.setPosition(new Vector2(0, -.6f));
		
		FixtureDef fDefFeet = new FixtureDef();
		fDefFeet.shape = shapeFeet;
		fDefFeet.density = 1;
		
		// Head
		PolygonShape shapeHead = new PolygonShape();
		shapeHead.setAsBox(.35f, .1f, new Vector2(0, .7f), 0);
		
		FixtureDef fDefHead = new FixtureDef();
		fDefHead.shape = shapeHead;
		fDefHead.isSensor = true;
		
		// Add them to the world
		body = b2World.createBody(bDef);
		body.createFixture(fDefBody);
		fixtureFeet = body.createFixture(fDefFeet);
		fixtureHead = body.createFixture(fDefHead);
	}

	@Override
	public void dispose() {
		b2World.destroyBody(body);
		aura.dispose();
	}

	@Override
	public void beginContact(Contact contact) {
		feetBeginContact(contact);
		headBeginContact(contact);
		bodyBeginContact(contact);
	}
	
	private void feetBeginContact(Contact contact) {
		Fixture other;
		
		if (contact.getFixtureA() == fixtureFeet)
			other = contact.getFixtureB();
		else if (contact.getFixtureB() == fixtureFeet)
			other = contact.getFixtureA();
		else
			return;
		
		// Check to see if other object is an obstacle
		Object userData = other.getBody().getUserData();
		if (userData instanceof Obstacle)
			canJump++;
	}
	
	private void headBeginContact(Contact contact) {
		Fixture other;
		
		if (contact.getFixtureA() == fixtureHead)
			other = contact.getFixtureB();
		else if (contact.getFixtureB() == fixtureHead)
			other = contact.getFixtureA();
		else
			return;
		
		Object userData = other.getBody().getUserData();
		if (userData instanceof GravityBlock) // The user has hit his head on a falling block, he is crushed!
			blocksOnHead++;
		
	}

	private void bodyBeginContact(Contact contact) {
		Fixture other;
		
		if (contact.getFixtureA().getBody() == body)
			other = contact.getFixtureB();
		else if (contact.getFixtureB().getBody() == body)
			other = contact.getFixtureA();
		else
			return;
		
		Object userData = other.getBody().getUserData();
		if (userData instanceof DangerBlock) // The user has hit his head on a falling block, he is crushed!
			touchingSpikes = true;
		
		if (userData instanceof Launcher)
			launchers++;
	}
	
	@Override
	public void endContact(Contact contact) {
		feetEndContact(contact);
		headEndContact(contact);
		bodyEndContact(contact);
	}
	
	private void feetEndContact(Contact contact) {
		Fixture other;
		
		// Check to see if Player's Feet are involved in collision
		if (contact.getFixtureA() == fixtureFeet) {
			other = contact.getFixtureB();
		} else if (contact.getFixtureB() == fixtureFeet) {
			other = contact.getFixtureA();
		} else {
			return;
		}
		
		// Check to see if other object is an obstacle
		Object userData = other.getBody().getUserData();
		if (userData instanceof Obstacle) {
			canJump--;
		}
	}

	private void headEndContact(Contact contact) {
		Fixture other;
		
		if (contact.getFixtureA() == fixtureHead)
			other = contact.getFixtureB();
		else if (contact.getFixtureB() == fixtureHead)
			other = contact.getFixtureA();
		else
			return;
		
		Object userData = other.getBody().getUserData();
		if (userData instanceof GravityBlock) // The user has hit his head on a falling block, he is crushed!
			blocksOnHead--;
	}
	
	private void bodyEndContact(Contact contact) {
		Fixture other;
		
		if (contact.getFixtureA().getBody() == body)
			other = contact.getFixtureB();
		else if (contact.getFixtureB().getBody() == body)
			other = contact.getFixtureA();
		else
			return;
		
		Object userData = other.getBody().getUserData();
		if (userData instanceof Launcher)
			launchers--;
	}
	
	// Unused
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {}

	public static class PlayerAccessor implements TweenAccessor<Player> {
		
		public static final int COLOR_ALPHA = 1;
		public static final int ANIM_PROGRESS = 2;

		@Override
		public int getValues(Player target, int tweenType, float[] returnValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				returnValues[0] = target.getColorAlpha();
				return 1;
			case ANIM_PROGRESS:
				returnValues[0] = target.getAnimProgress();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(Player target, int tweenType, float[] newValues) {
			switch (tweenType) {
			case COLOR_ALPHA:
				target.setColorAlpha(newValues[0]);
				break;
			case ANIM_PROGRESS:
				target.setAnimProgress(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}
	
}
