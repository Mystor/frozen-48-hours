package com.agenosworld.frozen.gameworld.player;

import com.agenosworld.frozen.GameLoop;
import com.agenosworld.frozen.gameworld.player.Player.Facing;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Disposable;

public class PlayerController implements InputProcessor, Disposable {
	
	/**
	 * Key Bindings for Movement
	 */
	public static final int left = Keys.A, right = Keys.D, jump = Keys.SPACE;

	private Player player;
	
	public PlayerController (Player p) {
		player = p;
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case left:
			player.startWalking(Facing.LEFT);
			return true;
		case right:
			player.startWalking(Facing.RIGHT);
			return true;
		case jump:
			player.tryJump();
			return true;
		case Keys.V:
			player.getWorld().killPlayer();
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case left:
			player.stopWalking(Facing.LEFT);
			return true;
		case right:
			player.stopWalking(Facing.RIGHT);
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public void dispose() {
		GameLoop.iManager.removeProcessor(this);
	}

	/*
	 * Not Used
	 */
	
	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
