package com.agenosworld.frozen.gameworld.player;

import com.agenosworld.frozen.gameworld.Animateable;
import com.agenosworld.general.Updatable;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Disposable;

public class AnimationAura implements Updatable, ContactListener, Disposable {
	
	// The parent Body to follow around
	private Player parent;
	
	// This object
	private Body body;
	private Fixture fixture;
	
	public AnimationAura(Player player) {
		this.parent = player;
		
		BodyDef bDef = new BodyDef();
		bDef.position.set(player.getBody().getPosition());
		bDef.type = BodyType.DynamicBody;
		bDef.gravityScale = 0;
		bDef.fixedRotation = true;
		bDef.allowSleep = false;
		
		CircleShape shape = new CircleShape();
		shape.setRadius(1.90f);
		
		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.isSensor = true;
		
		body = player.getB2World().createBody(bDef);
		fixture = body.createFixture(fDef);
	}

	@Override
	public void update(float delta) {
		Vector2 pos = parent.getBody().getPosition();
		body.setTransform(pos.x, pos.y, 0);
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture other;
		
		// Check to see if Aura is involved in collision
		if (contact.getFixtureA() == fixture) {
			other = contact.getFixtureB();
		} else if (contact.getFixtureB() == fixture) {
			other = contact.getFixtureA();
		} else {
			return;
		}
		
		// Check to see if other object can be animated
		Object userData = other.getBody().getUserData();
		
		if (userData instanceof Animateable) {
			Animateable target = (Animateable)userData;
			target.animate(); // Animate the object
		}
		
	}

	@Override
	public void endContact(Contact contact) {
		Fixture other;
		
		// Check to see if Aura is involved in collision
		if (contact.getFixtureA() == fixture) {
			other = contact.getFixtureB();
		} else if (contact.getFixtureB() == fixture) {
			other = contact.getFixtureA();
		} else {
			return;
		}
		
		// Check to see if other object can be frozen
		Object userData = other.getBody().getUserData();
		
		if (userData instanceof Animateable) {
			Animateable target = (Animateable)userData;
			target.freeze(); // Freeze the object
		}
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

	@Override
	public void dispose() {
		parent.getB2World().destroyBody(body);
		parent.getWorld().getContactManager().removeProcessor(this);
	}

}
