package com.agenosworld.frozen.gameworld;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;

public class BGColorManager {
	
	private float r = 0.7f, g = 0.7f, b = 0.7f, a = 1;
	
	public BGColorManager() {
		
	}
	
	public float getRed() {
		return r;
	}
	
	public void setRed(float v) {
		r = v;
	}
	
	public float getGreen() {
		return g;
	}
	
	public void setGreen(float v) {
		g = v;
	}
	
	public float getBlue() {
		return b;
	}
	
	public void setBlue(float v) {
		b = v;
	}
	
	public float getAlpha() {
		return a;
	}
	
	public void setAlpha(float v) {
		a  = v;
	}
	
	public void setRGB(float red, float green, float blue) {
		r = red;
		g = green;
		b = blue;
	}
	
	public void voidScreen() {
		Gdx.gl.glClearColor(r, g, b, a);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	}

	public static class BGColorManagerAccessor implements TweenAccessor<BGColorManager> {
		
		public static final int COLOR_RGB = 1;
		public static final int ALPHA = 2;

		@Override
		public int getValues(BGColorManager target, int tweenType,
				float[] returnValues) {
			switch(tweenType) {
			case COLOR_RGB:
				returnValues[0] = target.getRed();
				returnValues[1] = target.getGreen();
				returnValues[2] = target.getBlue();
				return 3;
			case ALPHA:
				returnValues[0] = target.getAlpha();
				return 1;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(BGColorManager target, int tweenType,
				float[] newValues) {
			switch(tweenType) {
			case COLOR_RGB:
				target.setRGB(newValues[0], newValues[1], newValues[2]);
				break;
			case ALPHA:
				target.setAlpha(newValues[0]);
				break;
			default:
				assert false;
				break;
			}
		}
		
	}
	
}
