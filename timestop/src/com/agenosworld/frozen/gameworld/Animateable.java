package com.agenosworld.frozen.gameworld;

public interface Animateable {

	/**
	 * Called when a TimeAura comes in contact with the object
	 */
	public void animate();

	/**
	 * Called when a TimeAura leaves contact with the object
	 */
	public void freeze();

}
