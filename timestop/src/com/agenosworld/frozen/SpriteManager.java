package com.agenosworld.frozen;

import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class SpriteManager {
	
	private static TextureAtlas atlas;
	
	public static void loadAtlas(FileHandle packFile) {
		atlas = new TextureAtlas(packFile);
		
		if (atlas != null) {
			System.out.println("Loaded Texture Atlas");
		}
	}
	
	/**
	 * Get the region defined by the name from the TextureAtlas
	 * @param name The name of the texture to retrieve
	 * @return The AtlasRegion representing the region retrieved from the TextureAtlas
	 */
	public static AtlasRegion getTexRegion(String name) {
		AtlasRegion region = atlas.findRegion(name);
		
		if (region == null)
			throw new NullPointerException("No such region of name "+name);
		return region;
	}
	
	/**
	 * Get the region set defined by the name from the TextureAtlas
	 * @param name The name of the texture set to retrieve
	 * @return The List of AtlasRegions representing the region retrieved from the TextureAtlas
	 */
	public static List<AtlasRegion> getTexRegions(String name) {
		List<AtlasRegion> regions = atlas.findRegions(name);
		
		if (regions == null)
			throw new NullPointerException("No such region set of name " + name);
		return regions;
	}
	
	/**
	 * Destroy the TextureAtlas
	 */
	public static void dispose() {
		atlas.dispose();
	}

}
