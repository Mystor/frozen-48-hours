package com.agenosworld.general;

public class StateManager extends GameState {
	
	private GameState currentState;
	
	private Disposer disposer;
	
	private Pair<Integer, Integer> lastResize;
	
	public StateManager(Disposer disposer) {
		this.disposer = disposer;
	}
	
	public void gotoState(GameState state) {
		if (currentState != null)
			disposer.queueToDispose(currentState);
		
		currentState = state;
		if (lastResize != null)
			currentState.resize(lastResize.l, lastResize.r);
	}

	@Override
	public void dispose() {
		currentState.dispose();
	}

	@Override
	public void update(float delta) {
		currentState.update(delta);
	}

	@Override
	public void resize(int width, int height) {
		currentState.resize(width, height);
		lastResize = new Pair<Integer, Integer>(width, height);
	}

	@Override
	public void render(boolean voidScr) {
		currentState.render(voidScr);
	}
	

}
