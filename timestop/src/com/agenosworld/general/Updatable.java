package com.agenosworld.general;

public interface Updatable {

	public void update(float delta);
	
}
