package com.agenosworld.general;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenManager;

public class ParticleEmitterMRL implements Updatable, Drawable {
	
	// Default values
	public TextureRegion visual;
	public Vector2 position = new Vector2(0, 0);
	public Vector2 dimensions = new Vector2(1, 1);
	public float velocity = 1;
	public float angle = 90;
	public float rotation = 0;
	public Color color = Color.WHITE;
	public float life = 1;
	public float spawnTime = 0.1f;
	public ParticleController controller;
	
	public Object userData;
	
	// Time which has elapsed since the last spawn
	private float timeSinceLastSpawn = 0;
	
	// A controller for the particles
	private TweenManager tManager;
	
	// The particles
	private Particle[] particles;
	
	public ParticleEmitterMRL(int maxParticles) {
		particles = new Particle[maxParticles];
		tManager = new TweenManager();
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		for (int i=0; i<particles.length; i++) {
			if (particles[i] != null && particles[i].isActive())
				particles[i].draw(batch);
		}
	}

	@Override
	public void update(float delta) {
		tManager.update(delta);
		
		timeSinceLastSpawn += delta;
		
		for (int i=0; i<particles.length; i++) {
			if (particles[i] != null) {
				if (!particles[i].isActive()) {
					// The particle has died
					
					if (controller != null)
						controller.unManageParticle(particles[i], this, tManager);
					
					particles[i] = null;
				} else {
					particles[i].update(delta);
				}
			} else {
				if (timeSinceLastSpawn >= spawnTime) {
					// Its time to spawn a new particle
					particles[i] = new Particle(visual, position, dimensions, velocity, angle, rotation, life, color);
					
					timeSinceLastSpawn = 0;
					
					// Get the controller to manage this particle
					if (controller != null)
						controller.manageParticle(particles[i], this, tManager);
				}
			}
		}
		
	}
	
	public static interface ParticleController {
		
		public void manageParticle(Particle particle, ParticleEmitterMRL emitter, TweenManager manager);
		
		public void unManageParticle(Particle particle, ParticleEmitterMRL emitter, TweenManager manager);
		
	}
	
	public class Particle implements Updatable, Drawable {
		
		private boolean active = true;
		private ParticleController controller;
		private TextureRegion visual;
		private Vector2 pos;
		private Vector2 vel;
		private Vector2 dim;
		private float rotation;
		private Color clr;
		private float life;
		private float currentLife = 0f;
		
		public Particle(TextureRegion visual, Vector2 position, Vector2 dimensions, float velocity, float angle, float rotation, float life, Color color) {
			this(visual, position.x, position.y, dimensions.x, dimensions.y, velocity, angle, rotation, life, color);
		}
		
		public Particle(TextureRegion visual, float x, float y, float width, float height, float velocity, float angle, float rotation, float life, Color color) {
			this.visual = visual;
			pos = new Vector2(x, y);
			vel = new Vector2(velocity, 0).rotate(angle);
			dim = new Vector2(width, height);
			this.rotation = rotation;
			clr = new Color(color);
			this.life = life;
		}
		
		// Accessors
		public Vector2 getPosition() {
			return pos;
		}
		
		public void setPosition(Vector2 pos) {
			this.pos = pos;
		}
		
		public float getVelocity() {
			return vel.len();
		}
		
		public void setVelocity(float v) {
			float angle = vel.angle();
			vel.set(v, 0).rotate(angle);
		}
		
		public Vector2 getDimensions() {
			return dim;
		}
		
		public void setDimensions(Vector2 d) {
			dim.set(d);
		}
		
		public float getRotation() {
			return rotation;
		}
		
		public void setRotation(float a) {
			rotation = a;
		}
		
		public float getAlpha() {
			return clr.a;
		}
		
		public void setAlpha(float a) {
			clr.a = a;
		}
		
		public Color getColor() {
			return new Color(clr);
		}
		
		public void setColor(float r, float g, float b) {
			clr.r = r;
			clr.g = g;
			clr.b = b;
		}
		
		public ParticleController getController() {
			return controller;
		}
		
		public void setController(ParticleController cont) {
			controller = cont;
		}
		
		public void setLife(float life) {
			this.life = life;
		}
		
		public boolean isActive() {
			return active;
		}
		
		public void deActivate() {
			active = false;
		}

		@Override
		public void draw(SpriteBatch batch) {
			// Record the color of the batch
			Color old = batch.getColor();
			
			// Color the batch and draw the particle
			batch.setColor(clr);
			batch.draw(visual, pos.x, pos.y, pos.x + (dim.x/2), pos.y + (dim.y/2), dim.x, dim.y, 1, 1, rotation);
			batch.setColor(old);
		}

		@Override
		public void update(float delta) {
			if (active) {
				Vector2 movement = new Vector2(vel);
				pos.y += movement.y * delta;
				pos.x += movement.x * delta;
				
				// If has died, deactivate
				currentLife += delta;
				if (currentLife >= life)
					deActivate();
			}
		}
		
	}
	
	public static class ParticleAccessor implements TweenAccessor<Particle> {
		
		public static final int VELOCITY = 1;
		public static final int DIMENSIONS = 2;
		public static final int ROTATION = 3;
		public static final int ALPHA = 4;
		public static final int COLOR = 5;
		public static final int POSITION = 6;

		@Override
		public int getValues(Particle target, int tweenType,
				float[] returnValues) {
			switch (tweenType) {
			case VELOCITY:
				returnValues[0] = target.getVelocity();
				return 1;
			case DIMENSIONS:
				Vector2 dim = target.getDimensions();
				returnValues[0] = dim.x;
				returnValues[1] = dim.y;
				return 2;
			case ROTATION:
				returnValues[0] = target.getRotation();
				return 1;
			case ALPHA:
				returnValues[0] = target.getAlpha();
				return 1;
			case COLOR:
				Color color = target.getColor();
				returnValues[0] = color.r;
				returnValues[1] = color.g;
				returnValues[2] = color.b;
				return 3;
			case POSITION:
				Vector2 pos = target.getPosition();
				returnValues[0] = pos.x;
				returnValues[1] = pos.y;
				return 2;
			default:
				assert false;
				return 0;
			}
		}

		@Override
		public void setValues(Particle target, int tweenType, float[] newValues) {
			switch (tweenType) {
			case VELOCITY:
				target.setVelocity(newValues[0]);
				break;
			case DIMENSIONS:
				target.setDimensions(new Vector2(newValues[0], newValues[1]));
				break;
			case ROTATION:
				target.setRotation(newValues[0]);
				break;
			case ALPHA:
				target.setAlpha(newValues[0]);
				break;
			case COLOR:
				target.setColor(newValues[0], newValues[1], newValues[2]);
				break;
			case POSITION:
				target.setPosition(new Vector2(newValues[0], newValues[1]));
				break;
			default:
				assert false;
				break;
			}
		}
		
	}
}
