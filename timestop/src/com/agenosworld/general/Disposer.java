package com.agenosworld.general;

import java.util.ArrayList;

import com.badlogic.gdx.utils.Disposable;

public class Disposer implements Updatable {
	
	private ArrayList<Disposable> toDispose = new ArrayList<Disposable>();
	
	public void queueToDispose(Disposable d) {
		if (d == null)
			throw new NullPointerException("Cannot dispose a null object");
		toDispose.add(d);
	}

	@Override
	public void update(float delta) {
		for (Disposable d : toDispose) {
			d.dispose();
		}
		
		toDispose.clear();
	}

}
