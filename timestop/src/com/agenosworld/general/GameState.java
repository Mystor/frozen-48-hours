package com.agenosworld.general;

import com.badlogic.gdx.utils.Disposable;

public abstract class GameState implements Disposable, Updatable, Resizable {
	
	/**
	 * Render the GameWorld
	 * @param voidScr Whether or not to void the screen before rendering
	 */
	public abstract void render(boolean voidScr);

}
