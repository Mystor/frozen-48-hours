package com.agenosworld.general;

public class Pair<L, R> {

	public final L l;
	public final R r;
	
	public Pair(L left, R right) {
		l = left;
		r = right;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == this) return true;
		if (!(other instanceof Pair))
			return false;
		
		Pair<?, ?> that = (Pair<?, ?>)other;
		
		return (this.l.equals(that.l) && this.r.equals(that.r));
	}
	
	@Override
	public String toString() {
		return "("+l.toString()+","+r.toString()+")";
	}
}
