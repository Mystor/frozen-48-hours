package com.agenosworld.general;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;

public class ContactManager implements ContactListener {
	
	private Array<ContactListener> processors = new Array<ContactListener>(4);
	
	public void addProcessor(int index, ContactListener processor) {
		processors.insert(index, processor);
	}
	
	public void removeProcessor(int index) {
		processors.removeIndex(index);
	}
	
	public void addProcessor (ContactListener processor) {
		processors.add(processor);
	}

	public void removeProcessor (ContactListener processor) {
		processors.removeValue(processor, true);
	}

	/**
	 * @return the number of processors in this multiplexer
	 */
	public int size() {
		return processors.size;
	}
	
	public void clear () {
		processors.clear();
	}

	public void setProcessors (Array<ContactListener> processors) {
		this.processors = processors;
	}

	public Array<ContactListener> getProcessors () {
		return processors;
	}

	@Override
	public void beginContact(Contact contact) {
		for (int i = 0, n = processors.size; i < n; i++)
			processors.get(i).beginContact(contact);
	}

	@Override
	public void endContact(Contact contact) {
		for (int i = 0, n = processors.size; i < n; i++)
			processors.get(i).endContact(contact);
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		for (int i = 0, n = processors.size; i < n; i++)
			processors.get(i).preSolve(contact, oldManifold);
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		for (int i = 0, n = processors.size; i < n; i++)
			processors.get(i).postSolve(contact, impulse);
	}
	
}
