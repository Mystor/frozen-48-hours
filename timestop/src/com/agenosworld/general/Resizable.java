package com.agenosworld.general;

public interface Resizable {

	public void resize(int width, int height);
	
}
