package com.agenosworld.timestop;

import com.agenosworld.frozen.GameLoop;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.imagepacker.TexturePacker;
import com.badlogic.gdx.tools.imagepacker.TexturePacker.Settings;

public class Main {
	public static void packTextures() {
		Settings settings = new Settings();
		settings.padding = 2;
		settings.maxWidth = 2048;
		settings.maxHeight = 1024;
		settings.minHeight = 32;
		settings.minWidth = 32;
		settings.rotate = false;
		settings.defaultFilterMag = Texture.TextureFilter.Linear;
		settings.defaultFilterMin = Texture.TextureFilter.Linear;
		settings.incremental = true;
		settings.stripWhitespace = false;
		
		TexturePacker.process(settings, "assets/sprites/raw", "assets/sprites");
	}
	
	public static void main(String[] args) {
		// Process the textures
		packTextures();
		
		if (args.length >= 1)
			GameLoop.debug = (args[0] == "-debug");
		
		// Run the game
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Frozen - A game by Mystor";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 600;
		
		new LwjglApplication(new GameLoop(), cfg);
	}
}
